import React from "react";
// node.js library that concatenates classes (strings)

// reactstrap components
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input,
  CustomInput,
  Badge,
  Card,
  CardHeader,
  CardFooter,
  Media,
  Pagination,
  PaginationItem,
  PaginationLink,
  Table,
  Container,
  Row,
  NavLink
} from "reactstrap";
import FormAccount from "../../components/modal/accountForm";
import FormFriend from "../../components/modal/friendForm"
// core components
import axios from "axios";
import { toast } from "react-toastify";

import { FaEdit } from "react-icons/fa";
import { MdDelete } from "react-icons/md";

import Header from "../../components/Headers/Header";
import "../../assets/css/dashboard.css";
import { connect } from "react-redux";
import * as actionTypes from "../../store/actions/action";


class Friend extends React.Component {
  state = {
    activeNav: 1,
    chartExample1Data: "data1",
    modal: false,
    friendmodal: false,
    accounts: [],
    editAccount:{},
    accountId:"",
    userId: "",
    action:'',
    otherUsers:[],
    friends:[]
  };

  getAccounts = async () => {
    console.log("this.state.accounts", this.state.accounts);

    const userData = JSON.parse(localStorage.getItem("userData"));
    this.setState({ userId: userData.id });
    console.log("All About Date", userData.createdAt);

    const resAccountData = await axios
      .get(`http://localhost:1337/accounts/${userData.id}`)
      .catch(error => {
        console.log("user not found", error);
        // toast.error(error.response.data.message);
      });
    if (resAccountData) {
      this.setState({ accounts: resAccountData.data.accounts });
      console.log("FETCHED accounts from didmount", resAccountData);
    } else {
      toast.error("No user exist");
    }
  };
  async componentDidMount() {
    await this.getAccounts();
  }

  togglemodal = () => {
    this.setState({ modal: !this.state.modal });
  };
  togglefriendmodal= () => {
    this.setState({ friendmodal: !this.state.friendmodal });
  };

  toggleNavs = (e, index) => {
    e.preventDefault();
    this.setState({
      activeNav: index,
      chartExample1Data:
        this.state.chartExample1Data === "data1" ? "data2" : "data1"
    });
    let wow = () => {
      console.log(this.state);
    };
    wow.bind(this);
    setTimeout(() => wow(), 1000);
    // this.chartReference.update();
  };

  addAccountHandler = async (e,name,balance) => {
    e.preventDefault();
    console.log("name",name);
    console.log("balance",balance);
    
    let resAddAccount = await axios
      .post(`http://localhost:1337/account/create/${this.state.userId}`, {
        name: name,
        balance: balance
      })
      .catch(error => {
        console.log("accounts not found", error.response.data);
        toast.error(error.response.data.error);
      });
    if (resAddAccount && resAddAccount.status === 200) {
      toast.success("Account added successfully");
      console.log('resAddAccount.data',resAddAccount.data.AccountData);
      this.props.onAccountCreate(resAddAccount.data.AccountData)

      await this.getAccounts();
    }
    this.togglemodal()
  };

  editAccountHandler = async (e,name,balance) => {
    e.preventDefault();
    console.log("name",name);
    console.log("balance",balance);


    let resEditAccount = await axios
      .put(`http://localhost:1337/account/edit/${this.state.editAccount.id}`, {
        name: name
      })
      .catch(error => {
        console.log("accounts not found", error.response.data.message);
        toast.error(error.response.data.message);
      });
    if (resEditAccount && resEditAccount.status === 200) {
      toast.success("Account edited successfully");
      await this.getAccounts();
    }
    this.togglemodal()
  };
  
  deleteAccHandler = async (e) => {
    e.preventDefault();
    console.log("${this.state.accountId}",this.state.accountId)
    let deleteAccount = await axios
      .delete(`http://localhost:1337/account/delete/${this.state.accountId}`)
      .catch(error => {
        // console.log("accounts not found while deleting", error.response.data.message);
        toast.error(error);
      });
    if (deleteAccount && deleteAccount.status === 200) {
      toast.success("Account deleted successfully");
      await this.getAccounts();
    }
  };
  getUsers = async () => {
console.log("ComES into GetUseerS&&&&&&")
    const userData = JSON.parse(localStorage.getItem("userData"));
    const resUserData = await axios
      .get(`http://localhost:1337/otherUsers/${userData.id}`)
      .catch(error => {
        console.log("user not found", error);
        // toast.error(error.response.data.message);
      });
      if(resUserData){
        this.setState({ otherUsers: resUserData.data.otherUsers });
        return resUserData
      }
   
  };
  getFriends = async () => {
    console.log("ComES into GetFrienDDDSSS----------")
        const userData = JSON.parse(localStorage.getItem("userData"));
        const resFriendData = await axios
          .get(`http://localhost:1337/getFriends/${userData.id}`)
          .catch(error => {
            console.log("friends not found", error);
            // toast.error(error.response.data.message);
          });
          if(resFriendData){
            this.setState({ friends: resFriendData[0].friends });
            return resFriendData
          }
       
      };
  setFriendEmail = e => {
    // On input change
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  addFriend = async (e,accountId) => {
    e.preventDefault();
    
    let resAddFriend = await axios
      .post(`http://localhost:1337/addFriend/${this.state.friend_email}`,{
        accountId:accountId
      })
      .catch(error => {
        console.log("accounts not found", error);
        toast.error(error);
      });
    if (resAddFriend.status === 200) {
      toast.success("Friend added successfully");
      await this.getAccounts();

    }
    this.togglefriendmodal()
  };

  // onChange = e => {
  //   // On input change
  //   this.setState({
  //     [e.target.name]: e.target.value
  //   });
  // };
  render() {
    console.log('Whole Stste',this.state);
    console.log('PROPS FROM REDUX',this.props.accounts);
    
    return (
      <>
        <Header />

        {/* Page content */}
        <Container className="mt--7" fluid>
          <Row>
            <div className="col">
              <Card className="shadow">
                <CardHeader className="border-0 d-flex">
                  <h3 className="mb-0">Account Details</h3>
                  <Button
                    color="primary"
                    className="add-account"
                    onClick={()=>{
                      this.setState({ editAccount: {} });
                      this.setState({action:'Add'})

                      this.togglemodal()
                      
                      }}
                  >
                    Add Account
                  </Button>
                </CardHeader>
                <Table className="align-items-center table-flush" responsive>
                  <thead className="thead-light">
                    <tr>
                      <th scope="col">Account</th>
                      <th scope="col">Budget</th>
                      <th scope="col">Friends</th>
                      <th scope="col">Actions</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.accounts.map(account => (
                      <tr>
                        <th scope="row">
                          <Media>
                            <span className="mb-0 text-sm">{account.name}</span>
                          </Media>
                        </th>
                        <td>{account.balance}</td>

                        <td>
                    {/* <span>{account.friends.map(data=>(data.name))}</span>  */}
                    <span>{account.friends.map(data=>{
                      let friends = [];
                      friends.push(data.name + " ")
                      return friends
                    })}</span> 

                        </td>
                        <td>
                          <span className="p-2">
                            <FaEdit
                              onClick={() => {
                                this.setState({ editAccount: account });
                                this.setState({action:'Edit'})

                                this.togglemodal();
                              }}
                              style={{ cursor: "pointer" }}
                            ></FaEdit>
                          </span>
                          <span className="p-2">
                            <MdDelete style={{ cursor: "pointer" }} 
                            onClick={async (e)=>{
                              await this.setState({ accountId: account.id });
                              this.deleteAccHandler(e);
                            }}/>
                          </span>
                        </td>
                        <td>
                        <Button
                    color="primary"
                    className="add-friend"
                    onClick={async ()=>{
                      await this.setState({ accountId: account.id });
                      this.togglefriendmodal()
                      }}
                  >
                    Add Friend
                  </Button>

                        </td>
                        <td>
                          <NavLink>
                        <Button
                    color="primary"
                    className="view" 
                  >
                    View
                  </Button>
                  </NavLink>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </Table>
                
                <CardFooter className="py-4">
                 
                </CardFooter>
              </Card>
            </div>
          </Row>
        </Container>

        
      </>
    );
  }
}
const mapStateToProps = state => {
  return {
    accounts: state.accounts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onAccountCreate: ({id, name, balance, owner}) =>
      dispatch({
        type: actionTypes.ADD_ACCOUNT,
        accountData: { id: id, name: name, balance: balance, owner: owner }
      }),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Friend);
