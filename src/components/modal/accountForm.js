import React,{useState, useEffect} from "react";

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input
} from "reactstrap";

const accountform = (props) => {
  console.log('props in MODAL',props);
  const [name, setName] = useState("")
  const [balance, setBalance] = useState("")

  useEffect(() => {
   
    if(props.action === 'Edit')
    {
      setName(props.editAccount.name);
      setBalance(props.editAccount.balance);
    }
    else{
      setName("");
      setBalance("");
    }
  },[props])
  return (
    <Modal isOpen={props.modal} toggle={props.toggleModal}>
      <ModalHeader toggle={props.toggleModal}>
         {props.action} Account
      </ModalHeader>
      <ModalBody>
        <Form>
          <FormGroup>
            <Label for="accountName">Account Name</Label>
            <Input
              placeholder="name"
              name="name"
              type="string"
              onChange={(e)=>setName(e.target.value)}
              value={name}
              required
            />
          </FormGroup>
        { props.action === 'Add' && <FormGroup>
            <Label for="accountBalance">Balance</Label>
            <Input
              placeholder="balance"
              name="balance"
              type="number"
              onChange={(e)=>setBalance(e.target.value)}
              value={balance}
              required
            />
          </FormGroup>}
          <Button
            type="submit"
            color="primary"
            onClick={e => {
              if(props.action === 'Add'){
              props.addAccountHandler(e,name,balance);
              }
              else{
              props.editAccountHandler(e,name,balance);
              }
            }}
          >
           {
             props.action === 'Add'?'ADD':'EDIT'
           }
          </Button>
          <Button color="secondary" 
          onClick={()=>{
            props.toggleModal()
            }}

          >
            Cancel
          </Button>
        </Form>
      </ModalBody>
    </Modal>
  );
};
export default accountform;