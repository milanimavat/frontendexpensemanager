import React,{useState, useEffect} from "react";
import axios from "axios";

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

const friendform = (props) => {
  console.log('props in friend MODAL',props);

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen(prevState => !prevState);

  useEffect(() => {
     props.getUsers()
  },[])

  console.log("OTHER===USERS",props.otherUsers)
  return (

    <Modal isOpen={props.friendmodal} toggle={props.togglefriendmodal}>
      <ModalHeader toggle={props.togglefriendmodal}>
          Add Friend
      </ModalHeader>
      <ModalBody>
      <Form>
          <FormGroup>
    <Dropdown isOpen={dropdownOpen} toggle={toggle}>
    <DropdownToggle caret>
      Select an email of Friend
      </DropdownToggle>
    <DropdownMenu right>
      {props.otherUsers.map(data=>( 
      <DropdownItem
      name='friend_email'
      value={data.email}
      onClick={e=>{props.setFriendEmail(e)}}
      >{data.email}</DropdownItem>
      ))}
    </DropdownMenu>
  </Dropdown>
</FormGroup>
<FormGroup>
<Button
            type="submit"
            color="primary"
            onClick={(e)=>{
              props.addFriend(e,props.accountId)
              }}
          >
             ADD
          </Button>
          <Button color="secondary" 
          onClick={()=>{
            props.togglefriendmodal()
            }}
          >
            Cancel
          </Button>
</FormGroup>

</Form>
      </ModalBody>
    </Modal>
  );
};
export default friendform;