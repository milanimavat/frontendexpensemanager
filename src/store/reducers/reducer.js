import * as actionTypes from '../actions/action';

const initialState = {
    users: [],
    accounts:[]

};

const reducer = ( state = initialState, action ) => {
    switch ( action.type ) {
        case actionTypes.CREATE_USER:
            const newUser = {
                id: action.userData.id, // not really unique but good enough here!
                name: action.userData.name,
                email: action.userData.email,
                password: action.userData.password

            }
            return {
                ...state,
                users: state.users.concat( newUser )
            }
        case actionTypes.REMOVE_PERSON:
            return {
                ...state,
                users: state.users.filter(user => user.id !== action.personId)
            }
            case actionTypes.ADD_ACCOUNT:
                const newAccount = {
                    id: action.accountData.id, // not really unique but good enough here!
                    name: action.accountData.name,
                    balance: action.accountData.balance,
                    owner: action.accountData.owner
    
                }
                return {
                    ...state,
                    accounts: state.accounts.concat( newAccount )
                }
    }
    return state;
};

export default reducer;