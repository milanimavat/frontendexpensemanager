import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { createStore } from 'redux';
import { Provider } from 'react-redux';

import "./assets/vendor/nucleo/css/nucleo.css";
import "./assets/vendor/@fortawesome/fontawesome-free/css/all.min.css";
import "./assets/scss/argon-dashboard-react.scss"

import AdminLayout from "./layouts/Admin.jsx";
import AuthLayout from "./layouts/Auth.jsx";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import reducer from './store/reducers/reducer'

const store = createStore(reducer);

ReactDOM.render(
  <Provider store={store}>
  <BrowserRouter>
  <React.Fragment>
    <Switch>
      <Route path="/user" render={props => <AdminLayout {...props} />} />
      <Route path="/auth" render={props => <AuthLayout {...props} />} />
      <Redirect from="/" to="/user/index" />
    </Switch>
    <ToastContainer autoClose={2000} />
    </React.Fragment>
  </BrowserRouter>
    </Provider>,
  document.getElementById("root")
);
