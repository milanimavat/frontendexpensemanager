import React from "react";
import axios from "axios";


// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Col
} from "reactstrap";
import { toast } from "react-toastify";

class Register extends React.Component {
  onSubmit = async e => {
    e.preventDefault();
    var user = await axios
      .post("http://localhost:1337/register", this.state)
      .catch(error => {
        toast.error(error);
      });

    if (user && user.status === 200) {
      console.log("registered user", user);
      toast.success('User created successfully')
      setTimeout(()=>{
        this.props.history.push("/auth/login/?" + user.data.user.id);
      },1500)
    }
  };

  onChange = e => {
    // On input change
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    
    return (
      <>
        <Col lg="6" md="8">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <span> Sign Up</span>
              </div>
              <Form
                role="form"
                method="POST"
                onSubmit={e => {
                  this.onSubmit(e);
                }}
              >
                <FormGroup>
                  <InputGroup className="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-hat-3" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Name"
                      type="text"
                      name="name"
                      onChange={this.onChange}
                      required
                    />
                  </InputGroup>
                </FormGroup>

                <FormGroup>
                  <InputGroup classNamFe="input-group-alternative mb-3">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email"
                      type="email"
                      name="email"
                      onChange={this.onChange}
                      required
                    />
                  </InputGroup>
                </FormGroup>

                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Password"
                      type="password"
                      name="password"
                      onChange={this.onChange}
                      minLength={8}
                      // {value: 6,errorMessage:"Your password must be of 8 character atleast"}
                      required
                    />
                  </InputGroup>
                </FormGroup>

                <div className="text-center">
                  <Button className="mt-4" color="primary" type="submit">
                    Create account
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
        </Col>
      </>
    );
  }
}

export default Register;
