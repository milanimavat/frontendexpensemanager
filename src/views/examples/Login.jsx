import React from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { connect } from "react-redux";
import * as actionTypes from "../../store/actions/action";
// reactstrap components
import {
  Button,
  Card,
  CardBody,
  FormGroup,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Row,
  Col
} from "reactstrap";

class Login extends React.Component {
  state = {
    email: null,
    password: null
  };

  onSubmit = async e => {
    e.preventDefault();

    // this.props.history.push('/auth/login')
    let res = await axios
      .post("http://localhost:1337/login", this.state)
      .catch(error => {
        console.log("user not found", error.response.data.message);
        toast.error(error.response.data.message);
      });
    
    if (res && res.status === 200) {
      console.log("user found", res.data);
      let user = JSON.stringify(res.data.user)
      localStorage.setItem('userData',user );
      this.props.onUserCreate(
        res.data.user.id,
        res.data.user.name,
        res.data.user.email,
        res.data.user.password
      );
      this.props.history.push("/user/index/?" + res.data.user.id);
    }
  };

  onChange = e => {
    // On input change
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    console.log('from redux loginnnn',this.props.users);

    return (
      <>
        <Col lg="5" md="7">
          <Card className="bg-secondary shadow border-0">
            <CardBody className="px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <small className="display-4">Sign In </small>
              </div>
              <Form
                role="form"
                method="POST"
                onSubmit={e => {
                  this.onSubmit(e);
                }}
              >
                <FormGroup className="mb-3">
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-email-83" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Email"
                      type="email"
                      name="email"
                      onChange={this.onChange}
                      required
                    />
                  </InputGroup>
                </FormGroup>
                <FormGroup>
                  <InputGroup className="input-group-alternative">
                    <InputGroupAddon addonType="prepend">
                      <InputGroupText>
                        <i className="ni ni-lock-circle-open" />
                      </InputGroupText>
                    </InputGroupAddon>
                    <Input
                      placeholder="Password"
                      type="password"
                      name="password"
                      onChange={this.onChange}
                      required
                    />
                  </InputGroup>
                </FormGroup>

                <div className="text-center">
                  <Button className="my-4" color="primary" type="submit">
                    Sign in
                  </Button>
                </div>
              </Form>
            </CardBody>
          </Card>
          <Row className="mt-3">
            <Col xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Forgot password?</small>
              </a>
            </Col>
            <Col className="text-right" xs="6">
              <a
                className="text-light"
                href="#pablo"
                onClick={e => e.preventDefault()}
              >
                <small>Create new account</small>
              </a>
            </Col>
          </Row>
        </Col>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onUserCreate: (id, name, email, password) =>
      dispatch({
        type: actionTypes.CREATE_USER,
        userData: { id: id, name: name, email: email, password: password }
      }),
    onRemovedPerson: id =>
      dispatch({ type: actionTypes.REMOVE_PERSON, personId: id })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
